# Sets the working directory
#setwd("D:\\seafile\\Seafile\\Uni\\Magistrale\\Machine learning e data mining\\imgvecs")
setwd("/home/simona/imgvecs")

# Imports helper functions
source("help.R")

# Imports all needed libraries
library(dplyr)
library(MVN)
library(dbscan)
library(stats)
library(outliers)

# How many samples to be kept from the CSV file
n_samples = 1000

# Loads the file and samples
imgvecs = sample_n(tbl_df(read.csv("img_vecs.csv",header = TRUE,sep = ",")),n_samples)
imgvecs

summary(imgvecs)

pairs(imgvecs[,3:7])

imgvecs_df = select(imgvecs,starts_with("el"))

# Removing outliers with MVN (PRE-PCA)
# https://www.rdocumentation.org/packages/MVN/versions/4.0/topics/mvOutlier, nella nuova versione non c'è più ma si può ottenere lo stesso con mvn
# result_quan = mvOutlier(imgvecs_df, qqplot = TRUE, method = "quan", label = TRUE)
# https://cran.r-project.org/web/packages/MVN/vignettes/MVN.pdf
# alpha = a numeric parameter controlling the size of the subsets over whichthe determinant is minimized. Allowed values for the alpha are between 0.5 and 1 and the default is 0.5.
# (n_samples = 1000)
# es. alpha = 0.3 329 non-outliers e 671 outliers
# es. alpha = 0.5 424 non-outliers e 576 outliers
# es. alpha = 0.7 446 non-outliers e 554 outliers
# es. alpha = 1 470 non-outliers e 530 outliers
# multivariatePlot = ’qq’ for chi-square Q-Q plot, ’persp’ for perspective plot, ’contour’for contour plot
result_quan = mvn(imgvecs_df, alpha=1, mvnTest="hz", multivariatePlot="qq", multivariateOutlierMethod = "quan", showOutliers=TRUE, showNewData=TRUE)
# result_quan e result_quan$* ci mettono un po' a visualizzare i risultati
# result_quan
result_quan$multivariateOutliers
newData_quan = result_quan$newData
newData_quan

pairs(result_quan$newData[,1:5])

# es. alpha = 0.3 351 non-outliers e 649 outliers
# es. alpha = 0.5 446 non-outliers e 554 outliers
# es. alpha = 0.7 476 non-outliers e 524 outliers
# es. alpha = 1 491 non-outliers e 509 outliers
result_adj_quan = mvn(imgvecs_df, alpha=1, mvnTest="hz", multivariatePlot="qq", multivariateOutlierMethod = "adj", showOutliers=TRUE, showNewData=TRUE)
# result_quan
result_adj_quan$multivariateOutliers
newData_adj = result_adj_quan$newData
newData_adj

pairs(result_adj_quan$newData[,1:5])

# PCA su newData
imgvec_pca_adj = prcomp(result_adj_quan$newData)

summary(imgvec_pca_adj)

imgvec_pca_adj_d = tbl_df(imgvec_pca_adj$x)
# How many pca elements to be kept (In this example around 50 elements gives 90% of original variance)
pca_index = 46

summary(imgvec_pca_adj_d)

pairs(imgvec_pca_adj_d[,1:5])

pca_adj_df = imgvec_pca_adj_d[,1:pca_index]

# PCA su dataset iniziale
imgvec_pca = prcomp(imgvecs_df)

summary(imgvec_pca)

imgvec_pca_d = tbl_df(imgvec_pca$x)
# How many pca elements to be kept (In this example around 50 elements gives 90% of original variance)
pca_index = 50

summary(imgvec_pca_d)

pairs(imgvec_pca_d[,1:5])

pca_df = imgvec_pca_d[,1:pca_index]

# Removing outliers with MVN (POST-PCA)
# es. alpha = 0.3 327 non-outliers e 673 outliers
# es. alpha = 1 664 non-outliers e 336 outliers
# multivariatePlot = ’qq’ for chi-square Q-Q plot, ’persp’ for perspective plot, ’contour’for contour plot
result_quan_post = mvn(pca_df, alpha=1, mvnTest="hz", multivariatePlot="qq", multivariateOutlierMethod = "quan", showOutliers=TRUE, showNewData=TRUE)
# result_quan e result_quan$* ci mettono un po' a visualizzare i risultati
# result_quan
result_quan_post$multivariateOutliers
newData_quan_post = result_quan_post$newData
newData_quan_post

pairs(result_quan_post$newData[,1:5])

# es. alpha = 0.3 337 non-outliers e 663 outliers
# es. alpha = 1 684 non-outliers e 316 outliers
result_adj_quan_post = mvn(pca_df, alpha=1, mvnTest="hz", multivariatePlot="qq", multivariateOutlierMethod = "adj", showOutliers=TRUE, showNewData=TRUE)
# result_quan
result_adj_quan_post$multivariateOutliers
newData_adj_post = result_adj_quan_post$newData
newData_adj_post

pairs(result_adj_quan_post$newData[,1:5])

# scores() function
# All of these tests assume that the data is normally distributed, with constant mean and variance. MA qui non è così!!!!!
# "z" calculates normal scores (differences between each value and the mean divided by sd), "t" calculates t-Student scores 
# (transformed by (z*sqrt(n-2))/sqrt(z-1-t^2) formula, "chisq" gives chi-squared scores (squares of differences between values 
# and mean divided by variance. For the "iqr" type, all values lower than first and greater than third quartile is considered, 
# and difference between them and nearest quartile divided by IQR are calculated. For the values between these quartiles, scores 
# are always equal to zero. "mad" gives differences between each value and median, divided by median absolute deviation.
# con prob=0.95 la maggior parte delle osservazioni ha almeno una colonna con valore fuori limite.
sc_chisq = scores(imgvecs_df, type="chisq", prob=0.98)  # beyond 98th %ile
newData_sc_chisq = imgvecs[rowSums(sc_chisq)==0,]
sc_z = scores(imgvecs_df, type="z", prob=0.98)  # beyond 98th %ile based on z-scores
newData_sc_z = imgvecs[rowSums(sc_z)==0,]
sc_t = scores(imgvecs_df, type="t", prob=0.98)  # beyond 98th %ile based on t-scores
newData_sc_t = imgvecs[rowSums(sc_t)==0,]
# è più che evidente che i risultati sono terribili

# scores() post pca
sc_chisq_post = scores(pca_df, type="chisq", prob=0.98)  # beyond 98th %ile
newData_sc_chisq_post = imgvecs[rowSums(sc_chisq_post)==0,]
sc_z_post = scores(pca_df, type="z", prob=0.98)  # beyond 98th %ile based on z-scores
newData_sc_z_post = imgvecs[rowSums(sc_z_post)==0,]
sc_t_post = scores(pca_df, type="t", prob=0.98)  # beyond 98th %ile based on t-scores
newData_sc_t_post = imgvecs[rowSums(sc_t_post)==0,]

# es score() con distribuzione normale
x = rnorm(1000)
sc_x_chisq = scores(x, type="chisq", prob=0.95)  # beyond 95th %ile
sum(sc_x_chisq)
sc_x_z = scores(x, type="z", prob=0.95)  # beyond 95th %ile based on z-scores
sum(sc_x_z)
sc_x_t = scores(x, type="t", prob=0.95)  # beyond 95th %ile based on t-scores
sum(sc_x_t)
# Si nota come qui vengano rilevati al massimo un centinaio di outliers circa su 1000, non più della metà del totale come accade invece col nostro dataframe.

# prova con scale() prima di scores()
scaled_imgvecs_df = scale(imgvecs_df)
scaled_imgvecs_df
sc_chisq_scaled_idf = scores(scaled_imgvecs_df, type="chisq", prob=0.98)  # beyond 98th %ile
newData_sc_chisq_scaled_idf = imgvecs[rowSums(sc_chisq_scaled_idf)==0,]
# ovviamente non è cambiato nulla ma valeva la pena provarci



