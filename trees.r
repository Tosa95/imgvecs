
# Sets the working directory
setwd("D:\\seafile\\Seafile\\Uni\\Magistrale\\Machine learning e data mining\\imgvecs")

source("help.R")

# Imports all needed libraries
library(dplyr)
library(tree)
library(randomForest)

# How many samples to be kept from the CSV file
n_samples = 2000

# Loads the file and samples
imgvecs = sample_n(tbl_df(read.csv("img_vecs_2.csv",header = TRUE,sep = ",")),n_samples)
imgvecs

train_test = train_test_split(imgvecs)
train = train_test$train
test = train_test$test

nrow(train)
nrow(test)

ctrl = tree.control(nrow(train),mincut = 1, minsize = 2)

tr = tree(lbl~.-ID,train)

plot(tr)
text(tr)
summary(tr)

pred = predict(tr, test, type="class")
table_and_accuracy(test,pred)

pruned_plot = cv.tree(tr, FUN = prune.misclass)
pruned_plot

plot(pruned_plot)

best = pruned_plot$size[which.min(pruned_plot$dev)]
pruned_tree = prune.misclass(tr, best = best)

pred = predict(pruned_tree, test, type="class")
table_and_accuracy(test,pred)

features = c(5,7,10,12,15,20,25,30)
ntrees = c(20,50,70,100,150,200,300)

best = find_rf_hyperpars(features,ntrees,retrials_per_pars = 10)

best

bag.cs = randomForest(lbl~., data=train, mtry=15, ntree=200)
summary(bag.cs)

pred = predict(bag.cs, test, type="class")
table_and_accuracy(test,pred)

