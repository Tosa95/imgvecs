# Sets the working directory
#setwd("D:\\seafile\\Seafile\\Uni\\Magistrale\\Machine learning e data mining\\imgvecs")
setwd("/home/simona/imgvecs")

# Imports helper functions
source("help.R")
source("outliers_methods.R")

# Imports all needed libraries
library(dplyr)
library(MVN)
library(dbscan)
library(stats)
library(outliers)

# How many samples to be kept from the CSV file
n_samples = 2000
# num clusters
num_clusters = 200
# num repetitions
n_rep = 15
# num minpoints for dbscan
minpts = 10

m <- c("cook", "cook+pca", "pca+cook", "dbscan", "dbscan+pca", "pca+dbscan", "km", "pca+km")
e <- c(0, 0, 0, 0, 0, 0, 0, 0)
cl_entropy <- data.frame(m,e)
names(cl_entropy) <- c("Method","Entropy") # variable names 
# cl_entropy[cl_entropy$Method == "cook",2]
# cl_entropy[cl_entropy$Method == "cook",2] = cl_entropy[cl_entropy$Method == "cook",2] + clust_entropy

# COOK + KMEANS
th = 1.5
cat("\nCOOK + KMEANS: \n")
for (i in 1:n_rep) {
  # Loads the file and samples
  imgvecs = sample_n(tbl_df(read.csv("img_vecs.csv",header = TRUE,sep = ",")),n_samples)
  imgvecs_df = select(imgvecs,starts_with("el"))
  
  imgvecs_num <- imgvecs[,-1]
  imgvecs_num$lbl = as.integer(imgvecs_num[["lbl"]])
  bool_cooks = cooks_method(imgvecs_num, th)
  imgvecs_cook = imgvecs[bool_cooks,]
  outliers_cook = imgvecs[!bool_cooks,]
  imgvecs_cook_df = select(imgvecs_cook,starts_with("el"))
  km_res = kmeans_result(imgvecs_cook_df, imgvecs_cook)
  
  cl_entropy[cl_entropy$Method == "cook",2] = cl_entropy[cl_entropy$Method == "cook",2] + km_res$clust_entr$sum_entropy[1]
  cat( i, km_res$clust_entr$sum_entropy[1], " ", nrow(outliers_cook), "\n")
}
cl_entropy[cl_entropy$Method == "cook",2] = cl_entropy[cl_entropy$Method == "cook",2]/n_rep
cat("\nCook + kmeans cl_entropy: ", cl_entropy[cl_entropy$Method == "cook",2])

# COOK + PCA + KMEANS
cat("\nCOOK + PCA + KMEANS: \n")
for (i in 1:n_rep) {
  imgvecs = sample_n(tbl_df(read.csv("img_vecs.csv",header = TRUE,sep = ",")),n_samples)
  imgvecs_df = select(imgvecs,starts_with("el"))
  
  imgvecs_num <- imgvecs[,-1]
  imgvecs_num$lbl = as.integer(imgvecs_num[["lbl"]])
  bool_cooks = cooks_method(imgvecs_num, th)
  imgvecs_cook = imgvecs[bool_cooks,]
  imgvecs_cook_df = select(imgvecs_cook,starts_with("el"))
  imgvecs_cook_pca_df = pca(imgvecs_cook_df)
  km_res = kmeans_result(imgvecs_cook_pca_df, imgvecs_cook)
  
  cl_entropy[cl_entropy$Method == "cook+pca",2] = cl_entropy[cl_entropy$Method == "cook+pca",2] + km_res$clust_entr$sum_entropy[1]
  cat( i, km_res$clust_entr$sum_entropy[1], "\n")
}
cl_entropy[cl_entropy$Method == "cook+pca",2] = cl_entropy[cl_entropy$Method == "cook+pca",2]/n_rep
cat("\nCook + pca + kmeans cl_entropy: ", cl_entropy[cl_entropy$Method == "cook+pca",2])

# DBSCAN + KMEANS
cat("\nDBSCAN + KMEANS: \n")
for (i in 1:n_rep) {
  imgvecs = sample_n(tbl_df(read.csv("img_vecs.csv",header = TRUE,sep = ",")),n_samples)
  imgvecs_df = select(imgvecs,starts_with("el"))
  
  kNNdistplot(imgvecs_df, k = minpts)
  eps = 0.17
  dbs = dbscan::dbscan(imgvecs_df,eps,minPts = minpts)
  imgvecs_dbscan <- imgvecs
  imgvecs_dbscan$clust = dbs$cluster
  imgvecs_no_noise <-imgvecs_dbscan[!(imgvecs_dbscan$clust==0),]
  imgvecs_no_noise_df = select(imgvecs_no_noise,starts_with("el"))
  km_res = kmeans_result(imgvecs_no_noise_df, imgvecs_no_noise)
  
  cl_entropy[cl_entropy$Method == "dbscan",2] = cl_entropy[cl_entropy$Method == "dbscan",2] + km_res$clust_entr$sum_entropy[1]
  cat( i, km_res$clust_entr$sum_entropy[1], "\n")
}
cl_entropy[cl_entropy$Method == "dbscan",2] = cl_entropy[cl_entropy$Method == "dbscan",2]/n_rep
cat("\nDbscan + kmeans cl_entropy: ", cl_entropy[cl_entropy$Method == "dbscan",2])

# DBSCAN + PCA + KMEANS
cat("\nDBSCAN + PCA + KMEANS: \n")
for (i in 1:n_rep) {
  imgvecs = sample_n(tbl_df(read.csv("img_vecs.csv",header = TRUE,sep = ",")),n_samples)
  imgvecs_df = select(imgvecs,starts_with("el"))
  
  kNNdistplot(imgvecs_df, k = minpts)
  eps = 0.17
  dbs = dbscan::dbscan(imgvecs_df,eps,minPts = minpts)
  imgvecs_dbscan <- imgvecs
  imgvecs_dbscan$clust = dbs$cluster
  imgvecs_no_noise <-imgvecs_dbscan[!(imgvecs_dbscan$clust==0),]
  imgvecs_no_noise_df = select(imgvecs_no_noise,starts_with("el"))
  imgvecs_dbscan_pca_df = pca(imgvecs_no_noise_df)
  km_res = kmeans_result(imgvecs_dbscan_pca_df, imgvecs_no_noise)
  
  cl_entropy[cl_entropy$Method == "dbscan+pca",2] = cl_entropy[cl_entropy$Method == "dbscan+pca",2] + km_res$clust_entr$sum_entropy[1]
  cat( i, km_res$clust_entr$sum_entropy[1], "\n")
}
cl_entropy[cl_entropy$Method == "dbscan+pca",2] = cl_entropy[cl_entropy$Method == "dbscan+pca",2]/n_rep
cat("\nDbscan + pca + kmeans cl_entropy: ", cl_entropy[cl_entropy$Method == "dbscan+pca",2])

# KMEANS
cat("\nKMEANS: \n")
for (i in 1:n_rep) {
  imgvecs = sample_n(tbl_df(read.csv("img_vecs.csv",header = TRUE,sep = ",")),n_samples)
  imgvecs_df = select(imgvecs,starts_with("el"))
  
  km_res = kmeans_result(imgvecs_df, imgvecs)
  
  cl_entropy[cl_entropy$Method == "km",2] = cl_entropy[cl_entropy$Method == "km",2] + km_res$clust_entr$sum_entropy[1]
  cat( i, km_res$clust_entr$sum_entropy[1], "\n")
}
cl_entropy[cl_entropy$Method == "km",2] = cl_entropy[cl_entropy$Method == "km",2]/n_rep
cat("\nKmeans cl_entropy: ", cl_entropy[cl_entropy$Method == "km",2])

# PCA + KMEANS
cat("\nPCA + KMEANS: \n")
for (i in 1:n_rep) {
  imgvecs = sample_n(tbl_df(read.csv("img_vecs.csv",header = TRUE,sep = ",")),n_samples)
  imgvecs_df = select(imgvecs,starts_with("el"))
  
  imgvecs_pca_df = pca(imgvecs_df)
  km_res = kmeans_result(imgvecs_pca_df, imgvecs)
  
  cl_entropy[cl_entropy$Method == "pca+km",2] = cl_entropy[cl_entropy$Method == "pca+km",2] + km_res$clust_entr$sum_entropy[1]
  cat( i, km_res$clust_entr$sum_entropy[1], "\n")
}
cl_entropy[cl_entropy$Method == "pca+km",2] = cl_entropy[cl_entropy$Method == "pca+km",2]/n_rep
cat("\nPca + kmeans cl_entropy: ", cl_entropy[cl_entropy$Method == "pca+km",2])

# PCA + COOK + KMEANS
cat("\nPCA + COOK + KMEANS: \n")
for (i in 1:n_rep) {
  imgvecs = sample_n(tbl_df(read.csv("img_vecs.csv",header = TRUE,sep = ",")),n_samples)
  imgvecs_df = select(imgvecs,starts_with("el"))
  
  imgvecs_pca_df = pca(imgvecs_df)
  pca_w_lbl <- imgvecs_pca_df
  pca_w_lbl$lbl = as.integer(imgvecs[["lbl"]])
  bool_cooks = cooks_method(pca_w_lbl, th)
  pca_cook_df = imgvecs_pca_df[bool_cooks,]
  imgvecs_pca_cook = imgvecs[bool_cooks,]
  imgvecs_pca_cook_df = select(imgvecs_pca_cook,starts_with("el"))
  km_res = kmeans_result(pca_cook_df, imgvecs_pca_cook)
  
  cl_entropy[cl_entropy$Method == "pca+cook",2] = cl_entropy[cl_entropy$Method == "pca+cook",2] + km_res$clust_entr$sum_entropy[1]
  cat( i, km_res$clust_entr$sum_entropy[1], "\n")
}
cl_entropy[cl_entropy$Method == "pca+cook",2] = cl_entropy[cl_entropy$Method == "pca+cook",2]/n_rep
cat("\nPca + cook + kmeans cl_entropy: ", cl_entropy[cl_entropy$Method == "pca+cook",2])

# PCA + DBSCAN + KMEANS
cat("\nPCA + DBSCAN + KMEANS: \n")
for (i in 1:n_rep) {
  imgvecs = sample_n(tbl_df(read.csv("img_vecs.csv",header = TRUE,sep = ",")),n_samples)
  imgvecs_df = select(imgvecs,starts_with("el"))
  
  imgvecs_pca_df = pca(imgvecs_df)
  kNNdistplot(imgvecs_pca_df, k = minpts)
  eps = 0.15
  dbs = dbscan::dbscan(imgvecs_pca_df,eps,minPts = minpts)
  imgvecs_pca_dbscan = imgvecs
  imgvecs_pca_dbscan$clust = dbs$cluster
  pca_dbscan_df <- imgvecs_pca_df[!(imgvecs_pca_dbscan$clust==0),]
  imgvecs_pca_dbscan <- imgvecs[!(imgvecs_pca_dbscan$clust==0),]
  imgvecs_pca_dbscan_df = select(imgvecs_pca_dbscan,starts_with("el"))
  km_res = kmeans_result(pca_dbscan_df, imgvecs_pca_dbscan)
  
  cl_entropy[cl_entropy$Method == "pca+dbscan",2] = cl_entropy[cl_entropy$Method == "pca+dbscan",2] + km_res$clust_entr$sum_entropy[1]
  cat( i, km_res$clust_entr$sum_entropy[1], "\n")
}
cl_entropy[cl_entropy$Method == "pca+dbscan",2] = cl_entropy[cl_entropy$Method == "pca+dbscan",2]/n_rep
cat("\nPca + dbscan + kmeans cl_entropy: ", cl_entropy[cl_entropy$Method == "pca+dbscan",2])

